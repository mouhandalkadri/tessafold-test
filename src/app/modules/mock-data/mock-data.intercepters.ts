import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { MockDataService } from "./mock-data.service";


@Injectable()
export class MockDataIntercepters implements HttpInterceptor {
    constructor(
        private readonly _mockGenerater: MockDataService
    ) { }


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.url.includes("list")) {
            return of(new HttpResponse({ body: this._mockGenerater.generate() }));
        }
        else {
            const splited = req.url.split("/");
            const id = parseInt(splited[splited.length - 1]);
            return of(new HttpResponse({ body: this._mockGenerater.generateSingle(id) }));
        }
    }

}