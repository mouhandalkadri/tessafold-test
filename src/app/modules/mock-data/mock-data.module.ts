import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { MockDataIntercepters } from "src/app/modules/mock-data/mock-data.intercepters";
import { MockDataService } from "./mock-data.service";


@NgModule({
    providers: [
        MockDataService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: MockDataIntercepters,
            multi: true
        }
    ],
})
export class MockDataModule { }