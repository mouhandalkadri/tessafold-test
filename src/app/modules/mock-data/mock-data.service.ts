import { Injectable } from "@angular/core";
import { Item } from "src/app/domain-modules/item/models/item.model";


@Injectable()
export class MockDataService {
    private readonly _ITEMS_COUNT = 35;


    generate() {
        return new Array(this._ITEMS_COUNT).fill(0).map(
            (_, index) => new Item({
                id: index + 1,
                value: `item ${index + 1}`,
                mill: new Date().getMilliseconds()
            })
        );
    }

    generateSingle(id: number) {
        return new Item({
            id: id,
            value: `item ${id}`,
            mill: new Date().getMilliseconds()
        });
    }
}