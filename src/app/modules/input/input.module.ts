import { NgModule } from "@angular/core";
import { PrimaryButtonInputDirective } from "./button-input/directives/primary-button-input.directive";
import { PrimaryTextInputDirective } from "./text-input/directives/primary-text-input.directive";


@NgModule({
    declarations: [
        PrimaryTextInputDirective,
        PrimaryButtonInputDirective
    ],
    exports: [
        PrimaryTextInputDirective,
        PrimaryButtonInputDirective
    ]
})
export class InputModule { }