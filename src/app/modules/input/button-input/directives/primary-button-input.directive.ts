import { Directive, ElementRef, OnInit } from "@angular/core";


@Directive({
    selector: "[tessa-primary-button-input]"
})
export class PrimaryButtonInputDirective implements OnInit {
    readonly CSS_CLASS_NAME = "tess_primary_button";


    constructor(
        private readonly _eleRef: ElementRef<HTMLInputElement>,
    ) {
    }

    ngOnInit(): void {
        this._eleRef.nativeElement.classList.add(this.CSS_CLASS_NAME);
    }
}