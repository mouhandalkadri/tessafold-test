import { Directive, ElementRef, OnDestroy, OnInit, Optional } from "@angular/core";
import { NgControl } from "@angular/forms";
import { Subject, takeUntil } from "rxjs";
import { TEXT_INPUT_ERROR_CLASS } from "./text-classes.const";


@Directive()
export abstract class TessaTextInputDirective implements OnInit, OnDestroy {
    private _onDestroy$ = new Subject<boolean>();

    constructor(
        protected eleRef: ElementRef<HTMLElement>,
        @Optional() protected readonly control?: NgControl
    ) { }

    ngOnInit(): void {
        this.control?.statusChanges?.pipe(
            takeUntil(this._onDestroy$)
        ).subscribe(
            () =>
                (this.control?.errors && this.control?.touched) ?
                    this.eleRef.nativeElement.classList.add(TEXT_INPUT_ERROR_CLASS) :
                    this.eleRef.nativeElement.classList.remove(TEXT_INPUT_ERROR_CLASS)
        );
    }

    ngOnDestroy(): void {
        this._onDestroy$.next(true);
    }

}