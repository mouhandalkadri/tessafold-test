import { Directive, ElementRef, OnInit, Optional } from "@angular/core";
import { NgControl } from "@angular/forms";
import { TessaTextInputDirective } from "../tessa-input.base.directive";


@Directive({
    selector: "[tessa-primary-text-input]"
})
export class PrimaryTextInputDirective extends TessaTextInputDirective implements OnInit {
    readonly CSS_CLASS_NAME = "tess_primary_input";


    constructor(
        public override eleRef: ElementRef<HTMLInputElement>,
        @Optional() public override readonly control?: NgControl
    ) {
        super(eleRef, control);
    }

    override ngOnInit(): void {
        super.ngOnInit();
        this.eleRef.nativeElement.classList.add(this.CSS_CLASS_NAME);
    }
}