import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Item } from '../../models/item.model';

import { ListItemComponent } from './list-item.component';

describe('ListItemComponent', () => {
    const mockItem = new Item({ id: 1, value: "1", mill: 1 });

    let component: ListItemComponent;
    let fixture: ComponentFixture<ListItemComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ListItemComponent]
        }).compileComponents();

        fixture = TestBed.createComponent(ListItemComponent);
        component = fixture.componentInstance;
        component.item = mockItem;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it(`should display value as ${mockItem.value}`, () => {
        const pLabel = fixture.nativeElement.querySelector("p") as HTMLParagraphElement;
        expect(pLabel.innerText).toContain(mockItem.value);
    })
});
