import { Injectable } from "@angular/core";
import { BehaviorSubject, filter, iif, map, of, switchMap, take, tap } from "rxjs";
import { Item } from "src/app/domain-modules/item/models/item.model";
import { ItemFetcher } from "../api-handlers/item.fetcher";


@Injectable()
export class ItemService {
    private _items$ = new BehaviorSubject<Item[] | undefined>(undefined);


    constructor(
        private readonly _fetcher: ItemFetcher
    ) { }


    getItemsList$() {
        return this._items$.asObservable().pipe(
            take(1),
            switchMap(
                cached => iif(
                    () => cached !== undefined,
                    of(cached).pipe(
                        filter((cached: Item[] | undefined): cached is NonNullable<Item[]> => cached !== undefined)
                    ),
                    this._fetcher.fetchList$().pipe(
                        take(1),
                        tap(items => this._items$.next(items))
                    )
                )
            )
        );
    }

    getSingleItem$(id: number) {
        return this._items$.asObservable().pipe(
            take(1),
            map(cached => cached?.find(item => item.id === id)),
            switchMap(
                item => iif(
                    () => item !== undefined,
                    of(item).pipe(
                        filter((cached: Item | undefined): cached is NonNullable<Item> => cached !== undefined)
                    ),
                    this._fetcher.fetchSingle$(id)
                )
            )
        )
    }
}