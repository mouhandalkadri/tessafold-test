import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { InputModule } from "src/app/modules/input/input.module";
import { ItemFetcher } from "./api-handlers/item.fetcher";
import { ListItemComponent } from './components/list-item/list-item.component';
import { ItemDetailsContainerComponent } from './containers/item-details-container/item-details-container.component';
import { SearchPageContainerComponent } from "./containers/search-page-container/search-page-container.component";
import { ItemsPageRoutingModule } from "./item-routing.modulei";
import { ItemDetailsResolver } from "./resolvers/item-details.resolver";
import { ItemService } from "./services/item.service";


@NgModule({
    imports: [
        InputModule,
        ItemsPageRoutingModule,
        CommonModule
    ],
    providers: [
        ItemFetcher,
        ItemService,
        ItemDetailsResolver
    ],
    declarations: [
        SearchPageContainerComponent,
        ListItemComponent,
        ItemDetailsContainerComponent
    ]
})
export class ItemsPageModule { }