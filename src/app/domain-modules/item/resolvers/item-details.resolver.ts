import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Item } from "../models/item.model";
import { ItemService } from "../services/item.service";


@Injectable()
export class ItemDetailsResolver implements Resolve<Item>{
    constructor(
        private readonly _itemService: ItemService
    ) { }

    resolve(route: ActivatedRouteSnapshot, _: RouterStateSnapshot) {
        const id = route.params["itemId"];
        return this._itemService.getSingleItem$(id);
    }

}