import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, Observable } from 'rxjs';
import { Item } from '../../models/item.model';


@Component({
  selector: 'tessa-item-details-container',
  templateUrl: './item-details-container.component.html',
  styleUrls: ['./item-details-container.component.scss']
})
export class ItemDetailsContainerComponent implements OnInit {
  item$!: Observable<Item>;

  constructor(
    private readonly _route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.item$ = this.getItemFromRoute$();
  }

  getItemFromRoute$() {
    return this._route.data.pipe(
      map(data => data["item"] as Item)
    );
  }

}
