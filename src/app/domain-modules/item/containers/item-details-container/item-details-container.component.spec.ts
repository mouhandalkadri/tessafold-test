import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from "@angular/router/testing";
import { of } from 'rxjs';
import { Item } from '../../models/item.model';
import { ItemDetailsContainerComponent } from './item-details-container.component';


describe('ItemDetailsContainerComponent', () => {
    const mockItem = new Item({ id: 1, value: "1", mill: 1 });
    let router: Router;
    let component: ItemDetailsContainerComponent;
    let fixture: ComponentFixture<ItemDetailsContainerComponent>;

    beforeEach(async () => {
        const mockActivatedRoute = {
            data: of({
                item: mockItem
            })
        };

        await TestBed.configureTestingModule({
            imports: [
                RouterTestingModule.withRoutes([
                    {
                        path: "item/search",
                        component: {} as any
                    }
                ])
            ],
            declarations: [ItemDetailsContainerComponent],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: mockActivatedRoute
                }
            ]
        }).compileComponents();
        router = TestBed.inject(Router);
        fixture = TestBed.createComponent(ItemDetailsContainerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it("should display id in header", () => {
        const h1Element = fixture.nativeElement.querySelector("h1") as HTMLElement;
        expect(h1Element.innerHTML).toContain(`Details Of item ${mockItem.id}`);
    });

    it("should display value", () => {
        const h1Element = fixture.nativeElement.querySelector("#value") as HTMLParagraphElement;
        expect(h1Element.innerHTML).toContain(`The value of the item is: ${mockItem.value}`);
    });

    it("should display milliseconds", () => {
        const h1Element = fixture.nativeElement.querySelector("#milliseconds") as HTMLParagraphElement;
        expect(h1Element.innerHTML).toContain(`The Milliseconds of creation is: ${mockItem.mill}`);
    });

    it("should navigate to search", fakeAsync(() => {
        const button = fixture.nativeElement.querySelector("button") as HTMLButtonElement;
        button.click();
        tick();
        expect(router.url).toBe("/item/search");
    }));

});
