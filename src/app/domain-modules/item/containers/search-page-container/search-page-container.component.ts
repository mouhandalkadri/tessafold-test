import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, map, switchMap, take } from 'rxjs';
import { Item } from 'src/app/domain-modules/item/models/item.model';
import { ItemService } from '../../services/item.service';

@Component({
  selector: 'tessa-search-page-container',
  templateUrl: './search-page-container.component.html',
  styleUrls: ['./search-page-container.component.scss']
})
export class SearchPageContainerComponent implements OnInit {
  items$ = new BehaviorSubject<Item[]>([]);
  viewItems$ = new BehaviorSubject<Item[]>([]);
  searchValue$ = new BehaviorSubject<string>("");

  constructor(
    private readonly _itemService: ItemService
  ) { }

  ngOnInit(): void {
    this._itemService.getItemsList$().pipe(
      take(1)
    ).subscribe(items => {
      this.items$.next(items);
      this.viewItems$.next(items);
      this.searchValue$.asObservable().pipe(
        switchMap(value => this.items$.pipe(
          map(items => items.filter(item => item.value.includes(value)))
        ))
      ).subscribe(filteredItems => this.viewItems$.next(filteredItems));

    })
  }

  onNewSearchValue(event: Event) {
    const value = (event.target as HTMLInputElement)?.value;
    this.searchValue$.next(value);
  }
}
