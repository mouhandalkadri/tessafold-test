import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { Item } from '../../models/item.model';
import { ItemService } from '../../services/item.service';
import { SearchPageContainerComponent } from './search-page-container.component';


describe('SearchPageContainerComponent', () => {
  let component: SearchPageContainerComponent;
  let fixture: ComponentFixture<SearchPageContainerComponent>;
  let router: Router;

  const mockItem = new Item({ id: 1, value: "1", mill: 1 });

  beforeEach(async () => {
    const mockItemService = {
      getItemsList$() {
        return of([{ ...mockItem }]);
      },
      getSingleItem$: (id: number) => {
        return of({ ...mockItem });
      }
    }


    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {
            path: `item/${mockItem.id}`,
            component: {} as any
          }
        ])
      ],

      declarations: [SearchPageContainerComponent],
      providers: [
        {
          provide: ItemService,
          useValue: mockItemService
        }
      ]
    }).compileComponents();

    router = TestBed.inject(Router);
    fixture = TestBed.createComponent(SearchPageContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("shoul show 0 values", () => {
    const input = fixture.nativeElement.querySelector("input") as HTMLInputElement;
    const inputContainer = fixture.nativeElement.querySelector("#input_container") as HTMLDivElement;
    const countP = inputContainer.querySelector("p");
    input.value = "SOME NON SENSE";
    input.dispatchEvent(new Event("input"));
    fixture.detectChanges();

    const notFoundDiv = fixture.nativeElement.querySelector("#no_items_container") as HTMLDivElement;
    const notFoundDivInnerText = (notFoundDiv.querySelector("p") as HTMLParagraphElement).innerText;

    expect(component.searchValue$.getValue()).toContain("SOME NON SENSE");
    expect(component.viewItems$.getValue().length).toEqual(0);
    expect(component.items$.getValue().length).toEqual(1);
    expect(notFoundDivInnerText).toContain("No Items Found :(");
    expect(countP?.innerText).toEqual("0 Item");
  });

  it("shoul show 1 value", () => {
    const input = fixture.nativeElement.querySelector("input") as HTMLInputElement;
    const inputContainer = fixture.nativeElement.querySelector("#input_container") as HTMLDivElement;
    const countP = inputContainer.querySelector("p");
    const notFoundDiv = fixture.nativeElement.querySelector("#no_items_container");

    input.value = "1";
    input.dispatchEvent(new Event("input"));
    fixture.detectChanges();

    expect(component.searchValue$.getValue()).toContain("1");
    expect(component.viewItems$.getValue().length).toEqual(1);
    expect(component.items$.getValue().length).toEqual(1);
    expect(countP?.innerText).toEqual("1 Item");
    expect(notFoundDiv).toBeNull();
  });

  it(`should navigate to item details of id ${mockItem.id}`, fakeAsync(() => {
    const itemList = fixture.nativeElement.querySelector("tessa-list-item");
    itemList.click();
    tick();
    expect(router.url).toBe(`/item/${mockItem.id}`);
  }))


});
