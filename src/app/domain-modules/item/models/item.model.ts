import { Properties } from "../../../utils/properties.utils";


export class Item {
    id!: number;
    value!: string;
    mill!: number;

    constructor(params: Properties<Item>) {
        Object.assign(this, params);
    }
}