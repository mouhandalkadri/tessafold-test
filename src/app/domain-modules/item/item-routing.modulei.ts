import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { ItemDetailsContainerComponent } from "./containers/item-details-container/item-details-container.component";
import { SearchPageContainerComponent } from "./containers/search-page-container/search-page-container.component";
import { ItemDetailsResolver } from "./resolvers/item-details.resolver";


const routes: Routes = [
    {
        path: "",
        redirectTo: "search",
        pathMatch: "full"
    },
    {
        path: "search",
        component: SearchPageContainerComponent,
        title: "Search Page"
    },
    {
        path: ":itemId",
        component: ItemDetailsContainerComponent,
        resolve: { item: ItemDetailsResolver },
        title: "Item Details"
    }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ItemsPageRoutingModule { }
