import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Item } from "src/app/domain-modules/item/models/item.model";
import { environment } from "src/environments/environment";


@Injectable()
export class ItemFetcher {
    constructor(
        private readonly _http: HttpClient
    ) { }


    fetchList$() {
        return this._http.get<Item[]>(`${environment.base}/list`);
    }

    fetchSingle$(id: number) {
        return this._http.get<Item>(`${environment.base}/single/${id}`);
    }
}